﻿using System;
using System.Collections.Generic;

namespace Graph
{
    public interface IGraph<T>
    {
        List<List<T>> RoutesBetween(T source, T target);
    }

    public class Graph<T> : IGraph<T>
    {
        private Dictionary<T, List<T>> AdjacentList { get; set; }

        public Graph(IEnumerable<ILink<T>> links)
        {
            AdjacentList = new Dictionary<T, List<T>>();

            foreach (Link<T> link in links)
            {
                if (AdjacentList.ContainsKey(link.Source))
                    AdjacentList[link.Source].Add(link.Target);
                else
                    AdjacentList[link.Source] = new List<T>() { link.Target };

                if (!AdjacentList.ContainsKey(link.Target))
                    AdjacentList[link.Target] = new List<T>();
            }
        }

        public List<List<T>> RoutesBetween(T source, T target)
        {
            Dictionary<T, bool> visited = new Dictionary<T, bool>();

            foreach (KeyValuePair<T, List<T>> kv in AdjacentList)
                visited[kv.Key] = false;

            List<T> paths = new List<T>();

            paths.Add(source);

            return getAllRoutesBetween(source, target, visited, paths, new List<List<T>>());
        }

        private List<List<T>> getAllRoutesBetween(T source, T target, Dictionary<T, bool> visited, List<T> localPath, List<List<T>> paths)
        {
            visited[source] = true;

            if (!source.ToString().Equals(target.ToString()))
            {
                foreach (T t in AdjacentList[source])
                {
                    if (!visited[t])
                    {
                        localPath.Add(t);
                        getAllRoutesBetween(t, target, visited, localPath, paths);
                        localPath.RemoveAt(localPath.Count - 1);
                    }
                }
            }
            else
            {
                paths.Add(localPath.GetRange(0, localPath.Count));
            }

            visited[source] = false;

            return paths;
        }
    }
}
