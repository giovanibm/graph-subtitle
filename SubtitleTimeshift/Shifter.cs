﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SubtitleTimeshift
{
    public class Shifter
    {
        async static public Task Shift(Stream input, Stream output, TimeSpan timeSpan, Encoding encoding, int bufferSize = 1024, bool leaveOpen = false)
        {
            using (var reader = new StreamReader(input, encoding, true, bufferSize, leaveOpen))
            using (var writer = new StreamWriter(output, encoding, bufferSize, leaveOpen))
            {
                while (!reader.EndOfStream)
                {
                    string line = await reader.ReadLineAsync();

                    if (line.Contains("-->"))
                    {
                        TimeSpan initial = TimeSpan.Parse(line.Substring(0, 12)).Add(timeSpan);
                        TimeSpan final = TimeSpan.Parse(line.Substring(18)).Add(timeSpan);

                        line = initial.ToString(@"hh\:mm\:ss\.fff") + " --> " + final.ToString(@"hh\:mm\:ss\.fff");
                    }

                    writer.WriteLine(line);
                }
            }
        }
    }
}
